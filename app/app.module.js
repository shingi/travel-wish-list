import angular from 'angular';

export let app = angular
  .module('travelWishList', [
    'ui.router',
    'ngSanitize'
  ]);
