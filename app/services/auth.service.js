import angular from 'angular';
import angularfire from 'angularfire'

function authService($firebaseAuth) {
    var firebaseAuthObject = $firebaseAuth();

    var service = {
        firebaseAuthObject: firebaseAuthObject,
        register: register,
        login: login,
        logout: logout,
        isLoggedIn: isLoggedIn,
        sendWelcomeEmail: sendWelcomeEmail
    };

    return service;

    function register(user) {
        return firebaseAuthObject.$createUserWithEmailAndPassword(user.email, user.password);
    }

    function login(user) {
        return firebaseAuthObject.$signInWithEmailAndPassword(user.email, user.password);
    }

    function logout() {
        firebaseAuthObject.$signOut();
    }

    function isLoggedIn() {
        return firebaseAuthObject.$getAuth();
    }

    function sendWelcomeEmail(emailAddress) {
        // firebaseDataService.emails.push({
        //     emailAddress: emailAddress
        // });
    }
}

export default angular.module('travelWishList.services', ['firebase', angularfire])
    .factory('authService', authService)
    .name;