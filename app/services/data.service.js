import angular from 'angular';

function dataService() {
    var root = firebase.database().ref();

    var service = {
        root: root,
        geofire: root.child('geofire'),
        locations: root.child('locations')
    };

    return service;
}

export default angular.module('travelWishList.dataService', ['firebase'])
    .factory('dataService', dataService)
    .name;