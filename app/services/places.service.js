import angular from 'angular';
import angularfire from 'angularfire';
import dataService from './data.service';

function placesService($firebaseArray, dataService, $firebaseObject) {

    var places = null;

    var geoFire = new GeoFire(dataService.geofire);

    var service = {
        Place: Place,
        getPlaceById: getPlaceById,
        getPlacesByUser: getPlacesByUser,
        reset: reset,
        setLocation: setLocation,
        geoFire: geoFire,
        vote: vote
    };

    return service;

    function Place() {
        this.title = '';
        this.latitude = '';
        this.longitude = '';
        this.votes = 0;
    }

    function getPlacesByUser(uid) {
        if (!places) {
            places = $firebaseArray(dataService.locations.orderByChild('userId').equalTo(uid));
        }
        return places;
    }

    function getPlaceById(id) {
        var data = $firebaseObject(dataService.locations.child(id));
        return data;
    }

    function setLocation(uid, location) {
        return geoFire.set(uid, location);
    }

    function vote(id, uid) {
        let promise = new Promise((resolve, reject) => {
            var place = $firebaseObject(dataService.locations.child(id));
            place.$loaded().then(function (data) {
                data.votes++;
                data.$save().then(function(ref) {
                    resolve(true);
                  }, function(error) {
                    reject(error);
                  });
              }).catch(function (error) {
                reject(error);
              });
          });
        return promise;
    }

    function reset() {
        if (places) {
            places.$destroy();
            places = null;
        }
    }
}

export default angular.module('travelWishList.placesService', ['firebase', angularfire, dataService])
    .factory('placesService', placesService)
    .name;