import './vendor'

import angular from 'angular';
import uirouter from 'angular-ui-router';
import angularfire from 'angularfire';

import routeConfig from './config/config';
import home from './features/home';
import user from './features/user';


angular.module('travelWishList', [uirouter, home, user, 'firebase'])
  .config(routeConfig)
  .config(function() {
    var config = {
      apiKey: "AIzaSyAOx5pcvFCf-NjPWR3Hx2SttG524_SAzyw",
      authDomain: "travel-wish-list-8f8b5.firebaseapp.com",
      databaseURL: "https://travel-wish-list-8f8b5.firebaseio.com",
      storageBucket: ""
    };
    firebase.initializeApp(config);
  })
  .constant('GOOGLE_MAPS_API_KEY', 'AIzaSyDxVrRk8vVnBwMpqjT8VGwXYoikj5AtD_4')
  .constant('GOOGLE_MAPS_URL', 'https://maps.googleapis.com/maps/api/js');