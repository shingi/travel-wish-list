routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
  $stateProvider
  .state('home', {
    url: '/',
    template: require('./home.tpl.html'),
    controller: 'HomeController',
    controllerAs: 'home'
  });
}