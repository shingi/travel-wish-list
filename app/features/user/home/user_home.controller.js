
UserHomeController.$inject = ['GOOGLE_MAPS_API_KEY', 'GOOGLE_MAPS_URL', '$window', 'NgMap', '$scope', 'placesService', 'user', '$rootScope'];

function UserHomeController(GOOGLE_MAPS_API_KEY, GOOGLE_MAPS_URL, $window, NgMap, $scope, placesService, user, $rootScope) {
  var vm = this;

  var thisMap;
  vm.googleMapsUrlWithKey = GOOGLE_MAPS_URL + '?key=' + GOOGLE_MAPS_API_KEY + '&libraries=places';
  vm.googleMapsUrl = GOOGLE_MAPS_URL;
  vm.zoomLevel = 8;
  vm.markers = [];
  vm.currentPosition = null;
  vm.map = null;
  vm.infoWindow = null;
  vm.placesService = null;
  vm.user = user;
  vm.userId = user.uid;
  vm.currentHits = [];
  var geoQuery;
  vm.hit;
  vm.isVoteInProgress = false;

  var mapIcon = {
    popular: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
    others: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
    loggedInUser: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
  };

  activate();

  function activate() {
    NgMap.getMap().then(function (map) {
      vm.map = map;
      vm.infoWindow = new google.maps.InfoWindow();
      vm.placesService = new google.maps.places.PlacesService(map);
      vm.currentPosition = map.getCenter();
      triggerGetLocations();

      google.maps.event.addListener(map, 'dragend', function (event) {
        vm.currentPosition = map.getCenter();
        triggerGetLocations();
      });
    });
  }

  function triggerGetLocations() {
    if (vm.currentPosition) {
      getLocations(100, [vm.currentPosition.lat(), vm.currentPosition.lng()]);
    }
  }

  vm.geoCallback = function () {
    if (!vm.map) return;
    vm.currentPosition = vm.map.getCenter();
  }

  function savePlace(place) {
    var locations = placesService.getPlacesByUser(vm.user.uid);

    locations.$add({
      title: place.name,
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      votes: 0,
      userId: vm.user.uid,
      timestamp: firebase.database.ServerValue.TIMESTAMP
    }).then(function (ref) {
      var id = ref.key;
      var loc = [place.geometry.location.lat(), place.geometry.location.lng()];
      placesService.setLocation(id, loc).then(function () {
      }, function (error) {
        console.log("Error setting GoeFire: " + error);
      });
    });
  }

  function getIcon(placeData) {
    let isPopular = placeData.votes >= 5;
    let iconUrl = isPopular ? 
      mapIcon.popular : 
      (placeData.userId === vm.userId ? 
        mapIcon.loggedInUser : 
        mapIcon.others);
    return iconUrl;
  }

  function createHit(data, distance) {
    let isPopular = data.votes >= 5;
    let canVote = data.userId === vm.userId;
    let hit = {
      id: data.$id,
      title: data.title,
      lat: data.latitude,
      lng: data.longitude,
      distance: distance,
      votes: data.votes,
      ownerId: data.userId,
      canVote: canVote,
      isPopular: isPopular,
      icon: getIcon(data)
    };
    return hit;
  }

  function getLocations(radius, coords) {
    if (typeof geoQuery !== "undefined") {
      geoQuery.updateCriteria({
        center: coords,
        radius: radius
      });
    } else {
      geoQuery = placesService.geoFire.query({
        center: coords,
        radius: radius
      });
    }

    geoQuery.on('key_entered', (key, location, distance) => {
      let place = placesService.getPlaceById(key);
      place.$loaded().then(function (data) {
        let hit = createHit(data, distance);
        vm.currentHits.push(hit);
      }).catch(function (error) {
        console.error("Error:", error);
      });
    });

    geoQuery.on("key_exited", function (key, location, distance) {
      console.log('Item: ' + key + ' at location ' + location + ' has exited.');
      var index = vm.currentHits.findIndex(h => h.id === key);
      if (index >= 0) {
        vm.currentHits.splice(index, 1);
        $rootScope.$apply();
      }
    });
  }

  function createMarker(place) {
    let markerOptions = {
      position: place.geometry.location,
      map: vm.map,
      optimized: true,
      clickable: true
    };

    let marker = new google.maps.Marker(markerOptions);

    savePlace(place);

    google.maps.event.addListener(marker, "click", function (place, marker) {
      return function () {
        if (place.vicinity) {
          vm.infoWindow.setContent(place.name + "<br/>" + place.vicinity);
        } else {
          vm.infoWindow.setContent(place.name);
        }
        //vm.infoWindow.setContent("Location: " + event.latLng.lat().toFixed(2) + ", " + event.latLng.lng().toFixed(2));
        vm.infoWindow.open(vm.map, marker);
      };
    }(place, marker));

    vm.markers.push(marker);
  }

  function nearByPlacesSearchCompleted(results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK && results.length > 0) {
      createMarker(results[0]);
    } else {
      // TODO: at the moment we are only creating markers for places known to Google places API
    }
  }

  function makePlacesRequest(latLng) {
    vm.placesService.nearbySearch({
      location: latLng,
      radius: 500
    }, nearByPlacesSearchCompleted);
  }

  vm.template = {
    cached: 'custom-cached-info-window-template.html'
  };

  vm.addMarker = function (event) {
    if (!vm.map) return;
    vm.currentPosition = event.latLng;
    makePlacesRequest(vm.currentPosition);
    vm.map.panTo(vm.currentPosition);
  }

  vm.showHitDetail = function(evt, hitId) {
    vm.hit = vm.currentHits.find(h => h.id === hitId);
    if (vm.hit) {
      vm.map.showInfoWindow('cached', this);
    }
  };

  vm.vote = function(hitId) {
    vm.isVoteInProgress = true;
    placesService.vote(hitId, vm.userId).then(result => {
      var index = vm.currentHits.findIndex(h => h.id === hitId);
      if (index >= 0) {
        vm.currentHits[index].votes++;
      }
      vm.isVoteInProgress = false;
      $rootScope.$apply();
    }, error => {
      console.log('Error: ', error);
    });
  }
}

export default UserHomeController