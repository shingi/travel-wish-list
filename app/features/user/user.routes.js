routes.$inject = ['$stateProvider'];

export default function routes($stateProvider) {
  $stateProvider
  .state('user', {
    url: '/user',
    template: require('./user.tpl.html'),
    controller: 'UserController',
    controllerAs: 'user',
    resolve: {
      user: ['authService', function(authService) {
        // TODO: redirect to home page if not authenticated
          return authService.firebaseAuthObject.$requireSignIn();
      }]
   }
  })
  .state('user.home', {
    url: '/home',
    template: require('./home/user_home.tpl.html'),
    controller: 'UserHomeController',
    controllerAs: 'vm',
    resolve: {
      user: ['authService', function(authService) {
          return authService.firebaseAuthObject.$requireSignIn();
      }]
   }
  });
}
  