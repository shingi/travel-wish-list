import './user.css';

import angular from 'angular';
import uirouter from 'angular-ui-router';
import ngMap from 'ngmap';

import authService from '../../services/auth.service';
import placesService from '../../services/places.service';

import routing from './user.routes';
import UserController from './user.controller';
import UserHomeController from './home/user_home.controller';

import layoutHeader from '../../components/layout/layout_header';

export default angular.module('travelWishList.user', [uirouter, layoutHeader, ngMap, authService, placesService])
  .config(routing)
  .controller('UserController', UserController)
  .controller('UserHomeController', UserHomeController)
  .name;