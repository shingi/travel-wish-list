import angular from 'angular';
import authService from '../../services/auth.service';

function layout_header() {

  LayoutHeaderController.$inject = ['$scope', '$state', '$window', 'authService', '$location'];

  function LayoutHeaderController($scope, $state, $window, authService, $location) {
    var vm = this;

    vm.email = "";
    vm.password = "";
    vm.auth = authService.firebaseAuthObject;

    vm.auth.$onAuthStateChanged(function (firebaseUser) {
      vm.firebaseUser = firebaseUser;
    });

    vm.signin = function () {
      authService.login({ email: vm.email, password: vm.password })
        .then(function (user) {
          $state.go('user.home');
        })
        .catch(function (error) {
          console.log("Authentication failed:", error);
          $location.path('/');
        })
    }

    vm.signout = function () {
      authService.logout();
      $location.path('/');
    }

  }

  return {
    restrict: 'E',
    replace: true,
    template: require('./layout_header.tpl.html'),
    transclude: true,
    controller: LayoutHeaderController,
    state: {},
    bindToController: true,
    controllerAs: 'vm'
  };
}

export default angular.module('directives.layout', [authService])
  .directive('layoutHeader', layout_header)
  .name;
