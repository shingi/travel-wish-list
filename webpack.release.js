const ExtractTextPlugin = require('extract-text-webpack-plugin');
const Config = require('webpack-config').Config;
const webpack = require('webpack');

module.exports = new Config()
    .extend('./webpack.web.js')
    .merge({
        plugins: [
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                },
                mangle: true,
                comments: false
            }),
        ],
        module: {
          loaders: [
              {
                  test: /\.css$/,
                  loader: ExtractTextPlugin.extract('style-loader', 'css-loader?minimize=true')
              },
              {
                  test: /\.less$/,
                  exclude: /node_modules/,
                  loader: ExtractTextPlugin.extract('style-loader', 'css-loader?minimize=true!less-loader')
              }
          ]
        }
    });
