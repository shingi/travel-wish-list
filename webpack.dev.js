var webpack = require('webpack');
var Config = require('webpack-config').Config;
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var history = require('connect-history-api-fallback');

module.exports = new Config()
    .extend('./webpack.debug.js')
    .merge({
        debug: true,
        devtool: '#source-map',
        plugins: [
            new BrowserSyncPlugin({
                host: 'localhost',
                port: 20010,
                server: {
                    baseDir: 'dist',
                    middleware: history()
                },
                ui: false,
                online: false,
                notify: false
            })
        ]
    });