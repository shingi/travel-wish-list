const path = require('path');
const webpack = require('webpack');
const Config = require('webpack-config').Config;
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ChunkHashReplacePlugin = require('chunkhash-replace-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = new Config()
    .extend('./webpack.base.js')
    .merge({
        plugins: [
          new webpack.ProvidePlugin({
            '_': 'underscore',
            'window._': 'underscore'
        }),
            new CleanWebpackPlugin(['dist'], {
              root: __dirname,
              verbose: true
            }),
            new ExtractTextPlugin('[name]-[chunkHash].css'),
            new HtmlWebpackPlugin({
                template: './index.html',
                inject: 'body',
                favicon: ''
            })
            // ,
            // new CopyWebpackPlugin(
            //   [
            //     { from: 'content/fonts/', to: 'fonts' },
            //     { from: 'content/images/', to: 'images' }
            //   ],
            //   {
            //     ignore: [],
            //     copyUnmodified: false
            //   })
        ]
    });
