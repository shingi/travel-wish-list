var webpack = require('webpack');
var failPlugin = require("webpack-fail-plugin");
var Config = require('webpack-config').Config;
var path = require('path');

module.exports = new Config()
    .merge({
        output: {
            filename: '[name]-[chunkHash].js',
            path: 'dist'
        },
        resolve: {
          root: __dirname,
          extensions: ['', '.js', '.json', '.less', '.css']
        },
        entry: {
          app: ['./app/app'],
          vendor: [
            "./content/css/bootstrap.css"
          ]
        },
        resolveLoader: {
            modulesDirectories: ["node_modules"]
        },
        plugins: [
            new webpack.NamedModulesPlugin(),
            failPlugin
        ],
        module: {
            loaders: [
              {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
              test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
              loader: 'file-loader'
            },
            {
              test: /\.tpl.html$/,
              exclude: /node_modules/,
              //loader: 'ngtemplate?relativeTo=' + (path.resolve(__dirname, './app')) + '/&module=travelWishList!html'
              loader: 'raw'
            },
            {
                test: /\.json$/,
                exclude: /node_modules/,
                loader: 'json-loader'
            }]
        }
    });
