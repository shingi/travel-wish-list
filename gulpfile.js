"use strict";

var gulp = require('gulp');

var del = require('del');

var gutil = require("gulp-util");

const webpack = require('webpack');

const webPackDebugConfig = require('./webpack.debug');
const webPackReleaseConfig = require('./webpack.release');

let activeConfig = webPackReleaseConfig;


gulp.task('default', ['webpack'], () => {
});

gulp.task("webpack", function(callback) {
    // run webpack
    webpack(activeConfig, function(err, stats) {
        if(err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString({
          colors: true,
          children: false,
          chunks: false,
          modules: false
        }));
        callback();
    });
});
