const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');
const Config = require('webpack-config').Config;

module.exports = new Config()
    .extend('./webpack.web.js')
    .merge({
        devtool: '#source-map',
        module: {
            loaders: [
                {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
                },
                {
                    test: /\.less$/,
                    exclude: /node_modules/,
                    loader: ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader')
                }
            ]
        }
    });
